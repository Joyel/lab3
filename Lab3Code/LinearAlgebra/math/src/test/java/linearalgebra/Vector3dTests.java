//Joyel Selvakumar, 2111271

package linearalgebra;

import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dTests {

    final double TOLERANCE = 0.0000001;

    @Test
    public void testCreation() {
        Vector3d firstVector = new Vector3d(3,4,5);

        assertEquals(3,firstVector.getX(),TOLERANCE);
        assertEquals(4,firstVector.getY(),TOLERANCE);
        assertEquals(3,firstVector.getZ(),TOLERANCE); // This is to assert a different value than what's actually expected for Z. It fails, so it's one sign that the test seems to work.
        assertEquals(5,firstVector.getZ(),TOLERANCE);
    }

    @Test
    public void testMagnitude() {
        Vector3d firstVector = new Vector3d(3,4,5);

        assertEquals(2.5590474,firstVector.magnitude(),TOLERANCE); // Another negative control with false expected value. Test fails, as it should.
        assertEquals(7.0710678,firstVector.magnitude(),TOLERANCE); // Can use Math.sqrt() instead for more precise numbers, but I wanted to demonstrate using a non-zero tolerance value
    }

    @Test
    public void testDotProduct() {
        Vector3d firstVector = new Vector3d(3,4,5);
        Vector3d secondVector = new Vector3d(6, 8, 10);

        assertEquals(256, firstVector.dotProduct(secondVector),0); // Another negative control like the previous tests. Fails, as expected.
        assertEquals(100, firstVector.dotProduct(secondVector),0);
    }

    @Test
    public void testAdd() {
        Vector3d firstVector = new Vector3d(3,4,5);
        Vector3d secondVector = new Vector3d(6, 8, 10);

        assertEquals(9, firstVector.add(secondVector).getX(), TOLERANCE);
        assertEquals(12, firstVector.add(secondVector).getY(), TOLERANCE);
        assertEquals(46, firstVector.add(secondVector).getZ(), TOLERANCE); // Another negative control like the previous tests. Fails, as expected.
        assertEquals(15, firstVector.add(secondVector).getZ(), TOLERANCE);
    }

    @Test // Alternate way of testing the returned Vector3d. One more line, and another object created, but the input to assertEquals may be easier to read.
    public void testAddAlternate() {
        Vector3d firstVector = new Vector3d(3,4,5);
        Vector3d secondVector = new Vector3d(6, 8, 10);
        Vector3d thirdVector = firstVector.add(secondVector);

        assertEquals(9, thirdVector.getX(), TOLERANCE);
        assertEquals(12, thirdVector.getY(), TOLERANCE);
        assertEquals(46, thirdVector.getZ(), TOLERANCE); // Another negative control like the previous tests. Fails, as expected.
        assertEquals(15, thirdVector.getZ(), TOLERANCE);
    }
}