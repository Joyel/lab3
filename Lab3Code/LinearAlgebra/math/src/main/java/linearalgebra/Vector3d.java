//Joyel Selvakumar, 2111271

package linearalgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double newX, double newY, double newZ) {
        this.x = newX;
        this.y = newY;
        this.z = newZ;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public double dotProduct(Vector3d partnerVector) {
        return ((this.x * partnerVector.getX()) + (this.y * partnerVector.getY()) + (this.z * partnerVector.getZ()));
    }

    public Vector3d add(Vector3d partnerVector) {
        Vector3d sumV = new Vector3d((this.x + partnerVector.getX()), (this.y + partnerVector.getY()),
                (this.z + partnerVector.getZ()));
        return sumV;
    }
}